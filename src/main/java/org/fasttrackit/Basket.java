package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$;

public class Basket extends Page{
    private List<Product> productsInBasket = new ArrayList<>();

    private float totalPrice = 0.0F;
    private final ElementsCollection distinctProduct = $$(".row a");

    public void addProduct(Product product){
        product.addToBasket();
        this.productsInBasket.add(product);
        this.totalPrice += Float.parseFloat(product.getPrice());
        System.out.println("Basket new total: " + totalPrice);
    }

    public int getNumberOfDistinctProduct() {
        return distinctProduct.size();
    }


    public List<Product> getProductInBasket() {
        return productsInBasket;
    }

    public float getTotalPrice() {
        return totalPrice;
    }
}
