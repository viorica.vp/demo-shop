package org.fasttrackit;

public class DemoShopPage extends Page{
    private Header header;

    public DemoShopPage() {
        this.header = new Header("guest");
    }

    public Header getHeader() {
        return header;
    }
}
