package org.fasttrackit;

public class ProductInBasket {
    private int quantity;
    private double totalPrice;
    private double unitPrice;
    private String productName;

    public ProductInBasket(Product product, int quantity) {
        this.productName = product.getName();
        this.unitPrice = Double.parseDouble(product.getPrice());
        this.totalPrice = unitPrice * quantity;
        this.quantity = quantity;

    }
}
