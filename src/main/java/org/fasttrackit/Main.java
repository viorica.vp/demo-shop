package org.fasttrackit;


import java.util.List;

public class Main {
    public static void main(String[] args) {
       DemoShopPage page = new DemoShopPage();
        page.openDemoShopApp();
        page.getHeader().clickOnTheLoginButton();
        String greetingsMsg = page.getHeader().getGreetingsMsg();
        System.out.println("Validate that the greetings message" + greetingsMsg);
        LoginModal modal = new LoginModal();
        modal.fillInUsername();
        modal.fillInPassword();
        modal.clickSubmitButton();
        Header header = new Header("beetle");
        String greetingsMessages = header.getGreetingsMsg();
        System.out.println("Validate that the greetings is: "+ greetingsMessages);


        System.out.println("........................");


        Product metalMauseProduct = new Product( "7","Practical Metal Mouse",  "9.99"  );
        metalMauseProduct.addToBasket();
        header = new Header("beetle", "1");
        page.getHeader().clickOnTheBasketIcon();
        Basket basketDetails = new Basket();
        basketDetails.addProduct(metalMauseProduct);
        List<Product> productsInBasket = basketDetails.getProductInBasket();
        System.out.println("Verify that the product one metal mause is in the basket" + productsInBasket.get(0).getName());
        System.out.println("Verify that the product practical metal mause price is 9.99: " + basketDetails.getProductInBasket().get(0).getPrice());
        System.out.println("Verify that tha total price is 9.99: " + basketDetails.getTotalPrice());



        page.refresh();
        Product metalMause2ndProduct = new Product("7","Practical Metal Mouse", "9.99"  );
        metalMause2ndProduct.addToBasket();
        Product softPizza = new Product("9","Gorgeous Soft Pizza", "19.99"  );
        softPizza.addToBasket();
        Basket basket2Details = new Basket();
        basket2Details.addProduct(metalMauseProduct);
        basket2Details.addProduct(softPizza);

    }
}