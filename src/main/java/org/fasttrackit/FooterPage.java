package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class FooterPage {
    private final SelenideElement resetButton = $(".fa-undo");

    public void resetPage() {
        this.resetButton.click();
    }
}
