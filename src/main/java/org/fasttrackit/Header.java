package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {
    private String loginButton = "Login button";


    private String greetingsMsg = "Hello Guest!";
    private final SelenideElement basketIcon = $(".fa-shopping-cart");
    private SelenideElement basketBadge = $(".shopping_cart_badge");

    public Header(String user) {
        this.greetingsMsg = "Hello" + user + "!";
        }

    public Header(String user, String basketItems) {
        this.greetingsMsg = "Hello" + user + "!";
    }
    public String getNumberOfProductInCart() {
        return this.basketBadge.text();
    }

    public void clickOnTheLoginButton() {
        System.out.println("Click on the" + loginButton);
    }

    public String getGreetingsMsg() {
        return greetingsMsg;
    }

    public void clickOnTheBasketIcon(){
        basketIcon.click();
    }
}