package org.fasttrackit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;

public class DemoShopAppEuTest {
    DemoShopPage page;

    @BeforeTest
    private DemoShopPage setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();

        Product metalMagicMause = new Product("7","Practical Metal Mouse",  "9.99" );
        metalMagicMause.addToBasket();
        return page;
    }
    @AfterMethod
    public void cleanup(){
        System.out.println("Cleaning up after the test.");
        FooterPage footer = new FooterPage();
        footer.resetPage();
    }

    @Test
    public void add_practical_metal_mouse_product_in_cart_number_of_product_in_cart_is_one() {
        ProductDetailsPage detailsPage = new ProductDetailsPage();
        detailsPage.clickOnTheAddToBasketButton();

        String numberOfProductInCart = page.getHeader().getNumberOfProductInCart();

        assertEquals(numberOfProductInCart, "1" , "Adding one product to cart, the cart badge is1");

        sleep(2 * 1000);

    }


    @Test
    public void add_two_practical_metal_mouse_products_in_cart_number_of_product_in_cart_is_two() {
        ProductDetailsPage detailsPage = new ProductDetailsPage();
        detailsPage.clickOnTheAddToBasketButton();
        detailsPage.clickOnTheAddToBasketButton();

        String numberOfProductInCart = page.getHeader().getNumberOfProductInCart();

        assertEquals(numberOfProductInCart, "3", "Adding two product to cart, the cart badge is 2");

        sleep(2 * 1000);
    }
}


