package org.fasttrackit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class AddTwoDifferentProductsToCartTest {


    @Test
    public void when_adding_metal_mouse_and_gorgeous_pizza_to_cart_two_products_are_in_cart() {
        DemoShopPage page = new DemoShopPage();
        page.openDemoShopApp();
        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.addToBasket();
        Product softPizza = new Product("9", "Gorgeous Soft Pizza", "19.99");
        softPizza.addToBasket();

        String numberOfProductInCart = page.getHeader().getNumberOfProductInCart();
        assertEquals(numberOfProductInCart, "2", "Adding 2 different products in cart");
    }


    @AfterMethod
    public void cleanup(){
        System.out.println("Cleaning up after the test.");
        FooterPage footer = new FooterPage();
        footer.resetPage();
    }



    @Test
    public void adding_metal_mouse_and_gorgeous_pizza_to_cart_navigate_to_cart_page_two_product_are_in_cart() {
        DemoShopPage page = new DemoShopPage();
        page.openDemoShopApp();

        Product metalMouse = new Product("7", "Practical Metal Mouse",  "9.99");
        metalMouse.addToBasket();
        Product softPizza = new Product("9", "Gorgeous Soft Pizza", "19.99");
        softPizza.addToBasket();

        page.getHeader().clickOnTheBasketIcon();

        Basket basketPage = new Basket();
    }
}